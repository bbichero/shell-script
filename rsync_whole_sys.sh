#!/bin/bash

# If script in run at root, execute command with sudo (for command with pipe '\')
if [ "$EUID" -eq 0 ]
then
	sudo="sudo"
fi

# Default conf
port=22
remote_user=
remote_host=
backup_path=

usage="$(basename "$0") [-h] [-p port -u remote_user -h remote_host -b backup_path] -- rsync whole system to remote directory
where:
    -p  ssh port
    -u  remote username
    -h  remote hostname or IPv4
    -b  backup path (Directory not disk)"

# Check arguments
while getopts ":p:u:i:b:h" opt; do
	case $opt in
		h)
			echo "$usage"
			exit 0
			;;
		p)
			port=$OPTARG
			# Check if value is numeric
			if [ "$port" -ne "$port" ] 2>/dev/null
			then
				exit 0
			fi
			;;
		u)
			remote_user=$OPTARG
			;;
		i)
			remote_host=$OPTARG
			;;
		b)
			backup_path=$OPTARG
			;;
    		\?)
    			echo "Invalid option: -$OPTARG" >&2
    			exit 1
    			;;
  		:)
    			echo "Option -$OPTARG requires an argument." >&2
    			exit 1
    			;;
	esac
done

# Check if all arguments was provided
if [ ${#remote_user} -eq 0 ]
then
	echo "No remote user given, abort"
	exit 1
fi

if [ ${#remote_host} -eq 0 ]
then
	echo "No remote host given, abort"
	exit 1
fi
if [ ${#backup_path} -eq 0 ]
then
	echo "No backup path given, abort"
	exit 1
fi

# Execute rsync command
rsync -aAXv --exclude={"/dev/*","/proc/*","/sys/*","/tmp/*","/run/*","/mnt/*","/media/*","/lost+found"} \
-e "ssh -p ${port} -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --del \
/ "${remote_user}"@"${remote_host}":"${backup_path}"
