##########################  NEW HOST INSTALLATION  ########################
# Script must be execute when you own a new host
# Or if you want to have fresh install of debian 8
# with kvm, rsync, lvm ready
#!/bin/bash

function is_ipv4()
{
	  if [ $DEBUG -eq 1 ]
	  then
		    echo "## ENTER IN is_ipv4 function test of $1 value ##"
	  fi
	  # IPv6 check 
	  #if [[ $s =~ ^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$ ]]; then
	  if [[ $1 =~ ^([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$ ]]
	  then
    		return 1
    else
        echo -e "${RED}Error: Bad IPv4 given.${NC}";
		    return 0;
	  fi
}

# Script must be execute with root user !!!
echo "You must authorize root login on remote host"

# Ask IP VM master
read -p "IPv4 host : " IPv4
while is_ipv4 "$IPv4"
do
	  read -p "IPv4 host : " IPv4
done

# Ask what name new user will be
read -p 'New user to add: ' ADD_USER

# Connection to remote host
ssh root@${IPv4} << EOF
# Add new user
adduser ${ADD_USER}

# Add new user to group sudo , root and adm
adduser ${ADD_USER} root
adduser ${ADD_USER} sudo
adduser ${ADD_USER} adm

# Change /etc/resolv.conf file content
echo "nameserver 208.67.222.222
nameserver 208.67.222.220" > /etc/resolv.conf

# Install usefull package
apt-get install sudo qemu-kvm libvirt-bin virt-manager git make vim

# Add new user to kvm group
adduser ${ADD_USER} kvm

# Disabled root ssh connection
sed -i.bak s/PermitRootLogin yes/PermitRootLogin without-password/g /etc/ssh/sshd_config

# Update package
apt-get update
EOF
