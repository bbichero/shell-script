#!/bin/bash

FILE_TRANSFERT=( $(sed -n "/sending incremental file list/,/Number of files/{/sending incremental file list/b;/Number of files/b;p}" /var/log/rsync.log) )

if [ ${#FILE_TRANSFERT[@]} != 0 ]
then
	echo "$(date +%Y_%m_%d_%HH%MM%S)" >> /var/log/rsync.lite.log
	printf "%s\n" "${FILE_TRANSFERT[@]}" >> /var/log/rsync.lite.log
	echo  >> /var/log/rsync.lite.log
fi
