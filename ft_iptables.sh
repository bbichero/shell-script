#!/bin/sh
# Settings
sudo iptables="/sbin/iptables -w" /sbin/iptables -w -L -n > /dev/null 2>&1 || sudo iptables="/sbin/iptables" INTERFACE="eth0" # External interface

# Flush all rules
sudo iptables -F
sudo iptables -F -t nat
sudo iptables -F -t mangle
sudo iptables -F -t raw
#sudo iptables -F -t security

# Erase all non-default chains
sudo iptables -X
sudo iptables -X -t nat
sudo iptables -X -t mangle
sudo iptables -X -t raw
#sudo iptables -X -t security

# Reset counters
sudo iptables -Z

# Block all traffic by default
sudo iptables -P INPUT DROP
sudo iptables -P FORWARD DROP
sudo iptables -P OUTPUT DROP

# Don't block traffic on loopback interface (lo)
sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -A OUTPUT -o lo -j ACCEPT

# Allow established connections TCP
sudo iptables -A INPUT -p tcp -m state --state ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A OUTPUT -p tcp -m state --state ESTABLISHED,RELATED -j ACCEPT

# Allow established connections UDP
sudo iptables -A INPUT -p udp -m state --state ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A OUTPUT -p udp -m state --state ESTABLISHED,RELATED -j ACCEPT

#
# ICMP
#

## Allow input pings (ICMP/0,8)
sudo iptables -A INPUT -p icmp --icmp-type 8 -j ACCEPT
sudo iptables -A OUTPUT -p icmp --icmp-type 0 -j ACCEPT

## Allow output pings (ICMP/0,8)
sudo iptables -A INPUT -p icmp --icmp-type 0 -j ACCEPT
sudo iptables -A OUTPUT -p icmp --icmp-type 8 -j ACCEPT

## destination unreachable, service unavailable (ICMP/3)
# Incoming & outgoing size negotiation, service or destination unavailability, final traceroute response
sudo iptables -A INPUT -p icmp --icmp-type 3 -j ACCEPT
sudo iptables -A OUTPUT -p icmp --icmp-type 3 -j ACCEPT

## source quench (ICMP/4)
# Incoming & outgoing requests to slow down (flow control)
sudo iptables -A INPUT -p icmp --icmp-type 4 -j ACCEPT
sudo iptables -A OUTPUT -p icmp --icmp-type 4 -j ACCEPT

## time exceeded (ICMP/11)
# Incoming & outgoing timeout conditions, also intermediate TTL response to traceroutes
sudo iptables -A INPUT -p icmp --icmp-type 11 -j ACCEPT
sudo iptables -A OUTPUT -p icmp --icmp-type 11 -j ACCEPT

## parameter problem (ICMP/12)
# Incoming & outgoing error messages
sudo iptables -A INPUT -p icmp --icmp-type 12 -j ACCEPT
sudo iptables -A OUTPUT -p icmp --icmp-type 12 -j ACCEPT

## SSH server (TCP/22)
sudo iptables -A INPUT -i $INTERFACE -p tcp -m state --state NEW,ESTABLISHED,RELATED --dport 22 --sport 1024:65535 -s <authorized-ip> -j ACCEPT
