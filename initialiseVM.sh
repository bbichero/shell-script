#!/bin/bash

# Script must be run as root, so check EUID in user's env
if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit 1
fi

iptables_repo="https://gist.github.com/2fdddd8fc0a62b7fb58b8cc9701f952b.git"

YEL='\033[1;33m'   # Yellow
ORA='\033[1;214m'  # Orange
GRE='\033[0;32m'   # Green
RED='\033[0;31m'   # Red
NC='\033[0m' 	   # No Color
DEBUG=0

## Initialise VM
# Change root password
# create account
# add sudoer user
# insert ssh key
# Install package
# install iptables
# change ssh port

# Ask new root password to edit
echo "First we need to change your root password"
read -ps "New root password: " root_passwd
while ${#root_passwd} -lt 6
do
	read -p "root password must contain at least 6 char: " root_passwd
done

# edit root password
echo "$root_passwd\n$root_passwd | passwd"
echo -e ${GRE}"root password changed"${NC}

# Create account for an user
echo -e ${YEL}"You need to create one account"${NC}
read -p "Create a new account, please enter an unsername: " new_username

# if username was empty ask to enter again username
while ${#new_username} -eq 0
do
	read -p "Create a new account, please enter a VALID unsername: " new_username	
done

# Create username whitout paswword
echo -e ${YEL}adduser $new_username --gecos "$new_username,,," --disabled-password${NC}
adduser $new_username --gecos "$new_username,,," --disabled-password

# Ask passowrd for user
read -p "Please enter a password for user $new_username: " user_passwd	

# if password was empty ask to enter again password
while ${#user_passwd} -lt 6
do
	read -p "Please enter a VALID password for user $new_username: " user_passwd	
done

echo "$user_passwd\n$user_passwd | passwd $new_username"
echo -e ${GRE}"$new_username password changed"${NC}

# make new username previously created a sudoer user
echo -e ${YEL}"Need to have at least one sudoer user, $new_username will be added to group sudo and root"${NC}
adduser $new_username sudo
echo -e ${YEL}"adduser $new_username sudo"${NC}
adduser $new_username root
echo -e ${YEL}"adduser $new_username root"${NC}

# Ask for add ssh key to user previously created
read -p "Do you want to add an ssh key for user $new_username ? (y/N) " ask_ssh_key
if [ ${#ask_ssh_key} -eq 0 ]; then
	$ask_ssh_key='y'
fi

while [ $ask_ssh_key != 'y' ] && [ $ask_ssh_key != 'n' ]
do
	read -p "Only 'y' or 'n' are accepted " ask_ssh_key
done

# Export locale in bashrc
echo -e ${YEL}"Export locale in bashrc"${NC}
echo "export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8" > /root/.bashrc
source /root/.bashrc
echo "export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8" > /home/$new_username/.bashrc

# If user answer yes
if [ $ask_ssh_key == 'y' ]
then
	read -p "Please enter you public ssh key here : " ssh_key

	while ${#new_username} -eq 0
	do
		read -p "Enter a valid ssh key please: " ssh_key
	done
	if [ -d /home/${new_username}/.ssh ]; then
		echo -e ${YEL}"mkdir /home/${new_username}/.ssh"${NC}
		mkdir /home/${new_username}/.ssh
		echo -e ${YEL}"chmod 700 /home/${new_username}/.ssh"${NC}
		chmod 700 /home/${new_username}/.ssh
	fi
	echo -e ${YEL}"echo $ssh_key > /home/${new_username}/.ssh/authorized_key"${NC}
	echo $ssh_key > /home/${new_username}/.ssh/authorized_key
	echo -e ${YEL}"chmod 644 /home/${new_username}/.ssh/authorized_key"${NC}
	chmod 644 /home/${new_username}/.ssh/authorized_key
fi

# Ask user to install package
read -p "Do you want to install packages ? (y/N) " ask_packages
if [ ${#ask_packages} -eq 0 ]; then
	$ask_packages='y'
fi

while [ $ask_package != 'y' ] && [ $ask_packages != 'n' ]
do
	read -p "Only 'y' or 'n' are accepted " ask_packages
done

# if user asnwer yes
if [ $ask_packages == 'y' ]
then
	# ask which package user want to install
	read -p "Which package(s) do you want to install ? " packages

	while ${#packages} -eq 0
	do
		read -p "Enter valid package name" 
	done
	apt update
	apt install $ask_packages
	ret=$(echo $?)

	# if ret is != 0 re-ask package
	while $ret -ne 0
	do
		apt install $ask_packages
		$ret=$(echo $?)
	done
fi

# Ask user for configure iptables
read -p "Do you want to configure iptables ? (y/N) " ask_iptables
if [ ${#ask_iptables} -eq 0 ]; then
	$ask_iptables='y'
fi

while [ $ask_iptables != 'y' ] && [ $ask_iptables != 'n' ]
do
	read -p "Only 'y' or 'n' are accepted " ask_iptables
done

if [ $ask_iptables == 'y' ]
then
	apt install git
	cd /root
	git clone ${iptables_repo} iptables
	if [ -f "/root/iptables/iptables_light.sh" ]
	then
		/root/iptables/iptables_light.sh
	else
		echo "file iptables_light.sh can't be found"
	fi
	echo -e ${RED}"No IPv4 has been configure"${NC}
	echo -e ${RED}"Uncomment line 240 and add your personnal home IPv4"${NC}
	echo "After that execute script"
	echo /root/iptables/iptables_light.sh
fi
echo ${GRE}"Everything is confgure, bye"${NC}
