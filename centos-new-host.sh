##########################  NEW HOST INSTALLATION  ########################
# Script must be execute when you own a new host
# Or if you want to have fresh install of centos 7.3
# with kvm, rsync, lvm ready
# Bridge centos 6 : http://www.techotopia.com/index.php/Creating_a_CentOS_6_KVM_Networked_Bridge_Interface
#!/bin/bash

KVM_DIR=/your/kvm/img/path

function is_ipv4()
{
	  if [ $DEBUG -eq 1 ]
	  then
		    echo "## ENTER IN is_ipv4 function test of $1 value ##"
	  fi
	  # IPv6 check 
	  #if [[ $s =~ ^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$ ]]; then
	  if [[ $1 =~ ^([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0-9]{1,2}|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$ ]]
	  then
    		return 1
    else
        echo -e "${RED}Error: Bad IPv4 given.${NC}";
		    return 0;
	  fi
}

# Script must be execute with root user !!!
echo "You must authorize root login on remote host"

# Ask IP VM
read -p "IPv4 host : " IPv4
while is_ipv4 "$IPv4"
do
	  read -p "IPv4 host : " IPv4
done

# Ask gateway host
read -p "Gateway host : " GATEWAY
while is_ipv4 ${GATEWAY}
do
	  read -p "Gateway host : " GATEWAY
done

# Ask what name new user will be
read -p 'New user to add: ' ADD_USER

# Connection to remote host
ssh root@${IPv4} << EOF
# Add new user
adduser ${ADD_USER}

# Add new user to group root and adm
gpasswd -a ${ADD_USER} root
gpasswd -a ${ADD_USER} adm

# Change /etc/resolv.conf file content
echo "nameserver 208.67.222.222
nameserver 208.67.222.220" > /etc/resolv.conf

# Install usefull package
yum -y install @virt* dejavu-lgc-* xorg-x11-xauth tigervnc \
libguestfs-tools policycoreutils-python bridge-utils

# Add new user to kvm and libvirt group
gpasswd -a ${ADD_USER} kvm
gpasswd -a ${ADD_USER} libvirt

# Active libvirt
systemctl enable libvirtd
systemctl start libvirtd

# Set polkit rule for kvm
echo "polkit.addRule(function(action, subject) {
    if (action.id == "org.libvirt.unix.manage" &&
        subject.isInGroup("kvm")) {
            return polkit.Result.YES;
        }
});" >> /etc/polkit-1/rules.d/49-polkit-pkla-compat.rules

# Active module
modprobe --first-time bridge

# Install bridge stuff
yum install bridge-utils -y

# Centos 7.3
echo "NAME=bridge
DEVICE=br0
TYPE=Bridge
IPADDR=37.187.78.130
GATEWAY=37.187.78.254
PREFIX=24
BOOTPROTO=none
ONBOOT=yes
DELAY=0" > /etc/sysconfig/network-scripts/ifcfg-br0

echo "DEVICE=eth0
TYPE=Ethernet
HWADDR=00:25:90:d5:fc:3e
BOOTPROTO=none
ONBOOT=yes
BRIDGE=br0" > /etc/sysconfig/network-scripts/ifcfg-eth0

# Create new interface for bridge centos 6.8
echo "DEVICE=br0
BOOTPROTO=static
IPADDR=${IPv4}
NETMASK=250.250.250.0
GATEWAY=${GATEWAY}
ONBOOT=yes
TYPE=Bridge
NM_CONTROLLED=no" > /etc/sysconfig/network-scripts/ifcfg-br0

# Copy old network interface file
cp /etc/sysconfig/network-scripts/ifcfg-eth0  /etc/sysconfig/network-scripts/ifcfg-eth0.bak

# Edit old interface
echo "DEVICE=eth0
BOOTPROTO=none
IPADDR=${IPv4}
NETMASK=255.255.255.0
ONBOOT=yes
NM_CONTROLLED=no
BRIDGE=br0" > /etc/sysconfig/network-scripts/ifcfg-eth0

EOF
