# shell-script (debian 8)

#### Before start on VM destination authorize only sshd from VM source
## If you don't have configure a mail VM, you can install exim4
Check list for rsync implementation<br />
On VM source AND VM destination

#### Edit /etc/ssh/sshd_config (Will be set to without-password later)
-PermitRootLogin no<br />
+PermitRootLogin yes

rsync installation on VM source AND VM dest
```
apt-get install rsync
```
#### RSA key exchange between VM mail > VM src AND VM src > VM dst
```
chmod 700 ssh_rsa_connection.sh
```
#### On VM mail:
```
./ssh_rsa_connection.sh <ip-VM-src>
```
#### On VM src:
#### Edit /etc/ssh/sshd_config
-PermitRootLogin yes<br />
+PermitRootLogin without-password
```
./ssh_rsa_connection.sh <ip-VM-dest>
```
#### On VM dest edit /etc/ssh/sshd_config
-PermitRootLogin yes<br />
+PermitRootLogin without-password

#### On VM mail
Put rsync_VM.sh in /root/ and modify ip value<br />
``chmod 700 /root/rsync_VM.sh``

#### Create crontab entrie:
```
crontab -e
```
```
0  0  *  *  * /root/rsync_VM.sh
```

### On source VM
#### Put backup.sh file in /root/ and modify ip value and exludes directories / files
```
chmod 700 /root/backup.sh
```

Test backup.sh
```
./backup.sh > /var/log/rsync.log
cat /var/log/rsync.log
```
#### Put parse_rsync.sh in /root/
```
chmod 700 parse_rsync.sh
```
Test parse_rsync.sh
```
./parse_rsync.sh
cat /var/log/rsync.lite.log
```

#### Add crontab entrie:
```
crontab -e
```
```
*/10 *  *  *  * /root/backup.sh > /var/log/rsync.log && /root/parse_rsync.sh
rm /var/log/rsync.lite.log
```
Modify something on VM source in /home<br />
Wait 10 min and check if /var/log/rsync.lite.log exist<br />

#### On VM mail
Test mail sending:<br />
```
cd /root
./rsync_VM.sh
```
