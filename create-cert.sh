#!/bin/bash

#https://jamielinux.com/docs/openssl-certificate-authority/sign-server-and-client-certificates.html

YEL='\033[1;33m'
GRE='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Check arg
if [ ${#1} -eq 0 ]
then
	echo "usage: ./create_cert.sh <site-name>"
	exit 1
else
	SITE_NAME=$1
fi

# Create private key
echo -e ${YEL}openssl genrsa -aes256 \
      -out intermediate/private/www."$SITE_NAME".key.pem 2048${NC}
openssl genrsa -aes256 \
      -out intermediate/private/www."$SITE_NAME".key.pem 2048

# Set permission of private key
echo -e ${YEL}chmod 400 intermediate/private/www."$SITE_NAME".key.pem${NC}
chmod 400 intermediate/private/www."$SITE_NAME".key.pem

# Create csr with intermediate key
echo -e ${YEL} openssl req -config intermediate/openssl.cnf \
      -key intermediate/private/www."$SITE_NAME".key.pem \
      -new -sha256 -out intermediate/csr/www."$SITE_NAME".csr.pem ${NC}
openssl req -config intermediate/openssl.cnf \
      -key intermediate/private/www."$SITE_NAME".key.pem \
      -new -sha256 -out intermediate/csr/www."$SITE_NAME".csr.pem

# Create cert with new csr
echo -e ${YEL} openssl ca -config intermediate/openssl.cnf \
      -extensions server_cert -days 375 -notext -md sha256 \
      -in intermediate/csr/www."$SITE_NAME".csr.pem \
      -out intermediate/certs/www."$SITE_NAME".cert.pem ${NC}
openssl ca -config intermediate/openssl.cnf \
      -extensions server_cert -days 375 -notext -md sha256 \
      -in intermediate/csr/www."$SITE_NAME".csr.pem \
      -out intermediate/certs/www."$SITE_NAME".cert.pem

# Set permission of cert
echo -e ${YEL}chmod 444 intermediate/certs/www."$SITE_NAME".cert.pem${NC}
chmod 444 intermediate/certs/www."$SITE_NAME".cert.pem

# Verifiy the certificate
echo -e ${YEL}openssl x509 -noout -text \
      -in intermediate/certs/www."$SITE_NAME".cert.pem${NC}
openssl x509 -noout -text \
      -in intermediate/certs/www."$SITE_NAME".cert.pem

echo -e ${YEL}openssl verify -CAfile intermediate/certs/ca-chain.cert.pem \
      intermediate/certs/www."$SITE_NAME".cert.pem${NC}
openssl verify -CAfile intermediate/certs/ca-chain.cert.pem \
      intermediate/certs/www."$SITE_NAME".cert.pem

echo -e "${GRE}Creation DONE${NC}"
echo "To deploy new certificate and private key use the new following files :"
echo ca-chain.cert.pem
echo www."$SITE_NAME".key.pem
echo www."$SITE_NAME".cert.pem
