#!/bin/bash
sudo rsync -avz -e "ssh -o StrictHostKeyChecking=no -o \
UserKnownHostsFile=/dev/null" --del --stats \
--exclude=/path/for/directory/to/exclude \
/home/ <ip-backup-vm>:/home/
